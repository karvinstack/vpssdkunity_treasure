using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
public class WebConnector : MonoBehaviour
{
    // Start is called before the first frame update


    //이거 함수 합치든 고치든 하기
    public AccountName AddressText;
    public string URL;

    public Text accountName;
    public string AccountAddress;
    void Awake()
    {
        string account = PlayerPrefs.GetString("Account");
        accountName.text = account;
        AccountAddress = account;
    }
    void Start()
    {
        AddressText = GameObject.Find("Text").GetComponent<AccountName>();
        StartCoroutine(Upload());
    }

    IEnumerator Upload()
    {
        WWWForm form = new WWWForm();
        form.AddField("ToAddress", AccountAddress);


        UnityWebRequest www = UnityWebRequest.Post(URL, form);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("complete");

        }



        // UnityWebRequest request = UnityWebRequest.Get(URL);

        // yield return request.SendWebRequest();

        // if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        // {
        //     Debug.Log(request.error);
        // }
        // else
        // {
        //     Debug.Log(request.result);
        // }


    }
}
