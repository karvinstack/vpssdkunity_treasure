using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AccountName : MonoBehaviour
{


    public Text accountName;
    public string AccountAddress;
    // Start is called before the first frame update
    void Start()
    {
        string account = PlayerPrefs.GetString("Account");
        accountName.text = account;
        AccountAddress = account;
        print(account);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
